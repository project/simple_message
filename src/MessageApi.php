<?php

namespace Drupal\simple_message;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\simple_message\Entity\Message;
use Drupal\simple_message\Entity\MessageInterface;
use Drupal\user\Entity\User;

/**
 * Class MessageApi.
 */
class MessageApi
{

  /**
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MessageApi object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager)
  {
    $this->entityTypeManager = $entity_type_manager;
  }

  function getDiscussionThreads($user)
  {

    $idsMessage = [];
    $querySender = Drupal::entityQueryAggregate('simple_message')
      ->accessCheck(FALSE)
      ->aggregate('created', 'MAX')
      ->condition('sender_user', $user->id())
      ->groupBy('sender_user')
      ->groupBy('receiver_user');
    $resultAsSender = $querySender->execute();

    $queryReceiver = Drupal::entityQueryAggregate('simple_message')
      ->accessCheck(FALSE)
      ->aggregate('created', 'MAX')
      ->condition('receiver_user', $user->id())
      ->groupBy('sender_user')
      ->groupBy('receiver_user');

    $resultAsReceiver = $queryReceiver->execute();
    $allData = array_merge($resultAsReceiver, $resultAsSender);
    $all = array_map(fn($itm) => $itm + ['id_msg' => $this->getMessageIdBySenderReceiverDate(
        $itm['sender_user'],
        $itm['receiver_user'],
        $itm['created_max'],
      )], $allData);

    usort($all, fn($a, $b) => $b['created_max'] <=> $a['created_max']);

    $result = [];
    foreach ($all as $item) {
      $id = $item['sender_user'] == $user->id() ? $item['receiver_user'] : $item['sender_user'];
      $result[$id] = [
        'id_msg' => $item['id_msg'],
        'created' => $item['created_max']
      ];
      if ($result[$id]['created'] < $item['created_max']) {
        $result[$id] = [
          'id_msg' => $item['id_msg'],
          'created' => $item['created_max']
        ];
      }

    }

    $finalResult = [];
    foreach ($result as $uid => $el) {
      $msg = Message::load($el['id_msg']);
      $user = User::load($uid);
      $finalResult[] = [
        'user' => $user,
        'message' => $msg,
      ];
    }

    return $finalResult;
  }

  function getMessageIdBySenderReceiverDate($senderId, $receiverId, $date)
  {

    $query = $this->entityTypeManager
      ->getStorage('simple_message')
      ->getQuery()->accessCheck(FALSE)
      ->condition('sender_user', $senderId)
      ->condition('receiver_user', $receiverId)
      ->condition('created', $date);
    $result = $query->execute();
    return array_pop($result);
  }

  /**
   * get user message count by status
   */
  function getUserMessageCountPerStatus($user, $status, $sender = null)
  {
    $query = $this->entityTypeManager
      ->getStorage('simple_message')
      ->getQuery()->accessCheck(FALSE);
    $query->condition('receiver_user', $user->id(), '=')
      ->condition('status', $status, '=');
    if ($sender) {
      $id = is_object($sender) ? $sender->id() : $sender;
      $query->condition('sender_user', $id, '=');
    }
    return $query->count()->execute();
  }

  /**
   * Update message status by sender
   */
  function setMessagesStatus($sender, $receiver, $status = MessageInterface::TYPE_RECEIVED)
  {
    $sender_id = is_object($sender) ? $sender->id() : $sender;
    $receiver_id = is_object($receiver) ? $receiver->id() : $receiver;
    $query = Drupal::database()
      ->update('simple_message')
      ->fields([
        'status' => $status
      ]);
    if ($sender) {
      $query->condition('sender_user', $sender_id);
    }
    if ($receiver) {
      $query->condition('receiver_user', $receiver_id);
    }
    if ($sender || $receiver) {
      $query->execute();
    }

    Cache::invalidateTags(['simple_message_status']);
    $this->entityTypeManager
      ->getStorage('simple_message')
      ->resetCache();
  }

  /**
   * Get last message by sender
   */
  function getLastMessageBySender($sender)
  {
    $sender_id = is_object($sender) ? $sender->id() : $sender;
    $query = $this->entityTypeManager
      ->getStorage('simple_message')
      ->getQuery()
      ->condition('sender_user', $sender_id)
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->range(0, 1);
    $ids = $query->execute();

    if (!empty($ids)) {
      return Message::load(array_pop($ids));
    }
    return null;
  }

}
