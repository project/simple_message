<?php

namespace Drupal\simple_message\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\simple_message\Entity\Message;
use Drupal\simple_message\Entity\MessageInterface;
use Drupal\simple_message\MessageApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Class MessageController.
 */
class MessageController extends ControllerBase
{

  /**
   * @var MessageApi
   */
  protected $simpleMessageApi;

  /**
   * display inbox messages
   */
  function messages($thread_lead_user)
  {
    $threads = $this->simpleMessageApi->getDiscussionThreads($this->currentUser());
    $form = '';
    $form_mode = 'thread_message';
    if (Drupal::routeMatch()->getRouteName() == 'simple_message.new_discussion') {
      $form = $this->getSimpleMessageForm('new_discussion', $this->currentUser());
      $form_mode = 'new_discussion_message';
    }
    $messageStatus = MessageInterface::TYPE_RECEIVED;
    if (isset($thread_lead_user)) {
      $args = serialize([$this->currentUser()->id(), $thread_lead_user->id()]);
      $messages = views_embed_view('simple_message_messages_inbox', 'block_1', $args);
      $messageStatus = MessageInterface::TYPE_READ;
      $form = $this->getSimpleMessageForm('send_message', $this->currentUser(), $thread_lead_user);
    }
    $this->simpleMessageApi->setMessagesStatus($thread_lead_user, $this->currentUser(), $messageStatus);

    $newDiscussionLink = [
      '#type' => 'link',
      '#title' => $this->t('+ New discussion'),
      '#url' => Url::fromRoute('simple_message.new_discussion'),
      '#access' => $this->currentUser()->hasPermission('start new discussion'),
      '#options' => [
        'attributes' => [
          'class' => ['btn-new-discussion'],
        ],
      ],
    ];

    return [
      "#theme" => 'simple_message_inbox',
      "#threads" => $threads,
      '#sender' => $this->currentUser(),
      '#current_thread_lead' => $thread_lead_user,
      "#messages" => $messages ?? '',
      "#form" => $form,
      "#form_mode" => $form_mode,
      "#new_discussion_link" => $newDiscussionLink,
      "#attached" => [
        'library' => ['simple_message/simple_message'],
        'drupalSettings' => [
          'simple_message' => [
            'thread_messages_count' => $this->config('simple_message.settings')->get('thread_messages_count'),
          ],
        ],
      ]
    ];
  }

  /**
   * Load message form per form mode
   */
  private function getSimpleMessageForm($formMode, $senderUser, $receiverUser = null)
  {
    $messageEntity = Message::create([
      'sender_user' => $senderUser->id(),
    ]);
    if ($receiverUser) {
      $messageEntity->set('receiver_user', $receiverUser->id());
    }
    $form = $this->entityTypeManager()
      ->getFormObject('simple_message', $formMode)
      ->setEntity($messageEntity);
    return $this->formBuilder()->getForm($form);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    $instance = parent::create($container);
    $instance->simpleMessageApi = $container->get('simple_message.message_api');
    return $instance;
  }

  /**
   * Remove message action
   */
  function removeMessage(Message $message)
  {
    if ($message->getOwnerId() !== $this->currentUser()->id()) {
      return throw new AccessDeniedHttpException();
    }
    if ($message->get('status')->value == Message::TYPE_READ) {
      $message->set('status', Message::TYPE_REMOVED_ONCE);
    } else {
      $message->set('status', Message::TYPE_REMOVED_TWICE);
    }
    $status = $message->save();
    return new JsonResponse(['status' => $status]);
  }

}
