<?php

namespace Drupal\simple_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Message entities.
 *
 * @ingroup simple_message
 */
interface MessageInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface
{

  const TYPE_SENT = 'snt';
  const TYPE_REMOVED_ONCE = 'rm1';
  const TYPE_REMOVED_TWICE = 'rm2';
  const TYPE_READ = 'rd';
  const TYPE_RECEIVED = 'rcv';
  const TYPE_HIDDEN = 'hdn';

  /**
   * Gets the Message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Message.
   */
  public function getCreatedTime();

  /**
   * Sets the Message creation timestamp.
   *
   * @param int $timestamp
   *   The Message creation timestamp.
   *
   * @return MessageInterface
   *   The called Message entity.
   */
  public function setCreatedTime($timestamp);

  public function isHidden();
}
