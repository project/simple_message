<?php

namespace Drupal\simple_message\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Message entities.
 */
class MessageViewsData extends EntityViewsData
{

  /**
   * {@inheritdoc}
   */
  public function getViewsData()
  {
    $data = parent::getViewsData();
    $data['simple_message']['table']['join']['users_field_data'] = [
      'field' => 'receiver_user',
      'left_field' => 'uid'
    ];

    return $data;
  }

}
