<?php

namespace Drupal\simple_message\Entity;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Message entity.
 *
 * @ingroup simple_message
 *
 * @ContentEntityType(
 *   id = "simple_message",
 *   label = @Translation("Message entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\simple_message\MessageListBuilder",
 *     "views_data" = "Drupal\simple_message\Entity\MessageViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_message\Form\MessageForm",
 *       "send_message" = "Drupal\simple_message\Form\MessageSendForm",
 *       "new_discussion" = "Drupal\simple_message\Form\MessageNewDiscussionForm",
 *       "add" = "Drupal\simple_message\Form\MessageForm",
 *       "edit" = "Drupal\simple_message\Form\MessageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\simple_message\MessageHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\simple_message\MessageAccessControlHandler",
 *   },
 *   base_table = "simple_message",
 *   translatable = FALSE,
 *   admin_permission = "administer simple_message entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "sender_user_id",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/message/{simple_message}",
 *     "add-form" = "/admin/structure/message/add",
 *     "edit-form" = "/admin/structure/message/{simple_message}/edit",
 *     "delete-form" = "/admin/structure/message/{simple_message}/delete",
 *     "collection" = "/admin/structure/message",
 *   },
 *   field_ui_base_route = "simple_message.settings"
 * )
 */
class Message extends ContentEntityBase implements MessageInterface
{

  use StringTranslationTrait;
  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'sender_user' => Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['sender_user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Sender'))
      ->setDescription(t('The user ID of sender of the Message entity.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['receiver_user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Receiver'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 2,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['message'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Message'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 512)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 3,
        'settings' => [
          'rows' => 2,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['attached'] = BaseFieldDefinition::create('file')
      ->setLabel('Attached file')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'credentialing_providers',
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'file_default',
        'weight' => -3,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'file',
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The Message entity status'))
      ->setSettings([
        'allowed_values' => self::AllStatus()
      ])
      ->setDefaultValue(MessageInterface::TYPE_SENT)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Get all available status.
   */
  public static function AllStatus()
  {
    return [
      MessageInterface::TYPE_SENT => t('Sent'),
      MessageInterface::TYPE_RECEIVED => t('Received'),
      MessageInterface::TYPE_READ => t('Read'),
      MessageInterface::TYPE_REMOVED_ONCE => t('Removed'),
      MessageInterface::TYPE_REMOVED_TWICE => t('Removed twice'),
      MessageInterface::TYPE_HIDDEN => t('Hidden'),
    ];
  }

  public function getFormattedCreatedDate()
  {
    $value = $this->get('created')->value;
    $format = Drupal::config('simple_message.settings')
      ->get('message_date_format');
    return date($format, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime()
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp)
  {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('sender_user')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('sender_user', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('sender_user', $account->id());
    return $this;
  }

  public function label()
  {
    $placeHolders = [
      '@sender' => $this->getOwner()->getDisplayName(),
      '@receiver' => $this->get('receiver_user')->entity->getDisplayName(),
    ];
    return t('Message form @sender to @receiver', $placeHolders);
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('sender_user')->entity;
  }

  public function getCacheTags()
  {
    return Cache::mergeTags(
      parent::getCacheTags(),
      ['simple_message_status']
    );
  }

  /**
   * Check the message is hidden
   */
  public function isHidden()
  {
    return in_array($this->get('status')->value, $this->hiddenStatus());
  }

  /**
   * Get all  available hidden status.
   */
  public function hiddenStatus()
  {
    return [
      MessageInterface::TYPE_REMOVED_TWICE,
      MessageInterface::TYPE_HIDDEN,
    ];
  }
}
