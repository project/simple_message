<?php

namespace Drupal\simple_message;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\simple_message\Entity\MessageInterface;

/**
 * Access controller for the Message entity.
 *
 * @see \Drupal\simple_message\Entity\Message.
 */
class MessageAccessControlHandler extends EntityAccessControlHandler
{

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
  {
    /** @var MessageInterface $entity */
    switch ($operation) {
      case 'view':
        if ($entity->isHidden()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished message entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published message entities');
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit message entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete message entities');
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL)
  {
    return AccessResult::allowedIfHasPermission($account, 'add message entities');
  }


}
