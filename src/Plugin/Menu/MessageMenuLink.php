<?php

namespace Drupal\simple_message\Plugin\Menu;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simple_message\Entity\MessageInterface;
use Drupal\simple_message\MessageApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test derivative with an unsafe string.
 */
class MessageMenuLink extends MenuLinkDefault
{
  use StringTranslationTrait;

  /**
   * @var MessageApi
   */
  protected $simpleMessageApi;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $static_override, MessageApi $simpleMessageApi)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
    $this->simpleMessageApi = $simpleMessageApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu_link.static.overrides'),
      $container->get('simple_message.message_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle()
  {
    $count = $this->simpleMessageApi->getUserMessageCountPerStatus(Drupal::currentUser(), MessageInterface::TYPE_SENT);
    if ($count == 0) {
      return $this->t("Messages");
    }
    if ($count > 99) {
      $count = '99+';
    }
    return $this->t("Messages (%count)", ['%count' => $count]);
  }

  public function getCacheTags()
  {
    return Cache::mergeTags(
      parent::getCacheTags(),
      ['simple_message_list', 'entity.memory_cache:simple_message']
    );
  }

  public function getCacheContexts()
  {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }

}
