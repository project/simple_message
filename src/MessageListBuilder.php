<?php

namespace Drupal\simple_message;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\simple_message\Entity\Message;


/**
 * Defines a class to build a listing of Message entities.
 *
 * @ingroup simple_message
 */
class MessageListBuilder extends EntityListBuilder
{

  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header['id'] = $this->t('Message ID');
    $header['sender'] = $this->t('Sender');
    $header['receiver'] = $this->t('Receiver');
    $header['message'] = $this->t('Message');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Send date');
    $header['changed'] = $this->t('Last change date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
    /* @var Message $entity */
    $row['id'] = Link::createFromRoute($entity->id(), 'entity.simple_message.canonical', ['simple_message' => $entity->id()]);
    $row['sender'] = $entity->get('sender_user')->entity ? $entity->get('sender_user')->entity->getAccountName() : '';
    $row['receiver'] = $entity->get('receiver_user')->entity ? $entity->get('receiver_user')->entity->getAccountName() : '';
    $row['message'] = Unicode::truncate($entity->get('message')->value, 100) . '...';
    $row['status'] = $entity->get('status')->value;
    $row['created'] = date('Y-m-d H:i:s', $entity->get('created')->value);
    $row['changed'] = date('Y-m-d H:i:s', $entity->get('changed')->value);
    return $row + parent::buildRow($entity);
  }

}
