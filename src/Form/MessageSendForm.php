<?php

namespace Drupal\simple_message\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Message edit forms.
 *
 * @ingroup simple_message
 */
class MessageSendForm extends MessageForm
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);
    $form['status']['#access'] = false;
    $form['attached']['#access'] = false;
    $form['receiver_user']['#access'] = false;
    $form['sender_user']['#access'] = false;
    $form['#attached']['library'][] = 'simple_message/simple_message_script';
    $form['actions']['submit']['#value'] = $this->t('Send');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $entityStatus = parent::save($form, $form_state);
    $form_state->setRedirect('simple_message.user.messages', ['thread_lead_user' => $this->entity->get('receiver_user')->entity->id()]);
    return $entityStatus;
  }

}
