<?php

namespace Drupal\simple_message\Form;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Form controller for Message edit forms.
 *
 * @ingroup simple_message
 */
class MessageNewDiscussionForm extends MessageForm
{
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);

    $form['status']['#access'] = false;
    $form['attached']['#access'] = false;
    $form['state']['#access'] = false;
    $form['sender_user']['#access'] = false;

    $receiver = \Drupal::request()->get('sender');
    if (isset($receiver)) {
      $form['receiver_user']["widget"][0]['target_id']['#access'] = false;
      $form['receiver_user']["widget"][0]['target_id']['#default_value'] = $receiver;
    }
    $form['actions']['submit']['#value'] = $this->t('Send');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $this->entity->set('sender_user', User::load($this->currentUser()->id()));
    parent::save($form, $form_state);
    $form_state->setRedirect('simple_message.user.messages', ['thread_lead_user' => $this->entity->get('receiver_user')->entity->id()]);
  }

}
