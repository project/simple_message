<?php

namespace Drupal\simple_message\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Message edit forms.
 *
 * @ingroup simple_message
 */
class MessageForm extends ContentEntityForm
{

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
    $characterLimit = $this->config('simple_message.settings')->get('message_character_limit');
    if ($characterLimit != -1 && $characterLimit < strlen($form_state->getValue('message')[0]['value'])) {
      $form_state->setErrorByName('message', $this->t('the message is is @limit character.', ['@limit' => $characterLimit]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Message sent'));
        break;
      default:
        $this->messenger()->addMessage($this->t('Message saved'));
    }
    $form_state->setRedirect('entity.simple_message.collection');
  }

}
