<?php

namespace Drupal\simple_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Entity\View;

/**
 * Class MessageSettingsForm.
 *
 * @ingroup simple_message
 */
class MessageSettingsForm extends ConfigFormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'simple_message_settings';
  }

  /**
   * Defines the settings form for Message entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);
    $form['show_status'] = [
      '#title' => $this->t('Show status on messages'),
      '#type' => 'checkbox',
      '#description' => $this->t('Show the check on the message for the status'),
      '#default_value' => $this->config('simple_message.settings')->get('show_status') ?? TRUE
    ];
    $form['message_date_format'] = [
      '#title' => $this->t('Date format on message'),
      '#type' => 'textfield',
      '#description' => $this->t('The date format on the message, use standard PHP date format'),
      '#default_value' => $this->config('simple_message.settings')->get('message_date_format') ?? 'Y-m-d H:i:s'
    ];
    $form['thread_messages_count'] = [
      '#title' => $this->t('Threads to display'),
      '#type' => 'number',
      '#size' => 6,
      '#description' => $this->t('Number of threads to display per load more click.'),
      '#default_value' => $this->config('simple_message.settings')->get('thread_messages_count') ?? 20
    ];
    $form['messages_count'] = [
      '#title' => $this->t('Messages to display'),
      '#type' => 'number',
      '#size' => 6,
      '#description' => $this->t('Number of messages to display per show more click.'),
      '#default_value' => $this->config('simple_message.settings')->get('messages_count') ?? 10
    ];
    $form['message_character_limit'] = [
      '#title' => $this->t('Messages character limit'),
      '#type' => 'number',
      '#size' => 6,
      '#description' => $this->t('the message character limit, set it to -1 for unlimited number of character in the message'),
      '#default_value' => $this->config('simple_message.settings')->get('message_character_limit') ?? 250
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);
    $this->config('simple_message.settings')
      ->set('show_status', $form_state->getValue('show_status'))
      ->set('message_date_format', $form_state->getValue('message_date_format'))
      ->set('thread_messages_count', $form_state->getValue('thread_messages_count'))
      ->set('messages_count', $form_state->getValue('messages_count'))
      ->set('message_character_limit', $form_state->getValue('message_character_limit'))
      ->save();

    $view = View::load('simple_message_messages_inbox');
    if ($view) {
      $displays = $view->get('display');
      if (isset($displays['default']['display_options']['pager'])) {
        $displays['default']['display_options']['pager']['options']['items_per_page'] = $form_state->getValue('messages_count');
      }
      $view->set('display', $displays);
      $view->save();
    }

  }

  protected function getEditableConfigNames()
  {
    return [
      'simple_message.settings',
    ];
  }
}
