<?php

/**
 * @file
 * Contains simple_message.module.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\simple_message\Entity\MessageInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_help().
 */
function simple_message_help($route_name, RouteMatchInterface $route_match)
{
  switch ($route_name) {
    // Main module help for the simple_message module.
    case 'help.page.simple_message':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Send private message between users') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function simple_message_theme()
{
  return [
    'simple_message' => [
      'render element' => 'elements',
    ],
    'simple_message_inbox' => [
      'variables' => [
        'threads' => null,
        'sender' => null,
        'current_thread_lead' => null,
        'messages' => null,
        'form' => null,
        'form_mode' => null,
        'new_discussion_link' => ''
      ]
    ]
  ];
}

/**
 * Implements hook_views_query_alter().
 */
function simple_message_views_query_alter(ViewExecutable $view, QueryPluginBase $query)
{

  if ($view->id() == 'simple_message_messages_inbox') {
    $args = unserialize($view->args[0]);
    $query->addWhere('conditions', 'simple_message.receiver_user', $args, 'in');
    $query->addWhere('conditions', 'simple_message.sender_user', $args, 'in');

  }
}

/**
 * Implements Implements hook_preprocess_HOOK().
 */
function simple_message_preprocess_simple_message(array &$variables)
{

  $variables['current_user'] = Drupal::currentUser();
  /** @var MessageInterface $message */
  $message = $variables['elements']['#simple_message'];

  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  $variables['content']['sender_id'] = $message->get('sender_user')->entity->id();
  $variables['content']['message_id'] = $message->id();
  $variables['content']['created'] = $message->getFormattedCreatedDate();
  if (Drupal::config('simple_message.settings')->get('show_status')) {
    $variables['status'] = $message->get('status')->value;
  }
}

/**
 * Implements template_preprocess_views_view().
 */
function simple_message_preprocess_views_view(&$variables)
{
  $view = $variables['view'];
  if ($view->id() == 'simple_message_messages_inbox') {
    $variables['header'][] = $variables['pager'];
    $variables['pager']['#access'] = FALSE;
  }
}
