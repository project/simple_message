(function ($, Drupal, drupalSettings, once) {

  function setupLoadMore() {
    $("#sm-load-more").hide();
    let itemsToShow = drupalSettings.simple_message.thread_messages_count;
    let totalItems = $(".thread-container").length;
    if (totalItems != 0 && totalItems > itemsToShow) {
      $("#sm-load-more").show();
    }


    $(".thread-container").hide(); // Hide all items
    $(".thread-container:lt(" + itemsToShow + ")").show(); // Show the first 5

    $("#sm-load-more").on('click', function (e) {
      e.preventDefault();
      let currentlyVisible = $(".thread-container:visible").length;
      $(".thread-container:lt(" + (currentlyVisible + itemsToShow) + ")").fadeIn();

      if ($(".thread-container:visible").length >= totalItems) {
        $(this).hide(); // Hide "Load More" when all items are shown
      }
    });
  }

  Drupal.behaviors.simple_messge = {
    attach: function (context, settings) {
      $(document).ready(function () {
        setupLoadMore();

        $(once('sm-remove-msg', 'a.simple-message-remove')).on('click', function (e) {
          e.stopPropagation();
          e.preventDefault();
          var el = this;
          if (confirm('Do you realy want to remove this message ?')) {
            $.get($(this).attr('href'))
              .then(function (data) {
                $(el).parents('blockquote').html(Drupal.t('Message removed'));
              })
          }

        });

        $(once('sm-remove-msg', '.search-thread')).on('input', function (e) {
          let input = $(this);
          if ($(this).val() == '') {
            setupLoadMore();
          } else {
            $(".thread-container").show();
            $("#sm-load-more").hide();
          }
          $('.sm-threads .thread-user-name').each(function (index, el) {
            if ($(el).html().includes(input.val())) {
              $(el).parents('.thread-container').removeClass('visually-hidden');
            } else {
              $(el).parents('.thread-container').addClass('visually-hidden');
            }
          });
        });


      });
    }
  };

})(jQuery, Drupal, drupalSettings, once);
